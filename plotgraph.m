function[fig] = plotgraph(signum,FOLDERNAME,FILENAME,Xname,Xunit,Yname,Yunit,axisfont,lbfont)

    DATA= CSV2MAT(signum,FOLDERNAME,[FILENAME,'.csv']);
    [scale_x,scale_unitx]=scale_xy(DATA(1,1));
    [scale_y,scale_unity]=scale_xy(DATA(1,2));
    lbx=[Xname,'/',scale_unitx,Xunit];
    lby=[Yname,'/',scale_unity,Yunit];
    
    xmax=max(max(DATA(:,1:2:2*signum-1)));
    xmin=min(min(DATA(:,1:2:2*signum-1)));
    ymax=max(max(DATA(:,2:2:2*signum)));
    ymin=min(min(DATA(:,2:2:2*signum)));
    fig=figure;
    for i=1:2:2*signum
        plot(DATA(:,i)*scale_x,DATA(:,i+1)*scale_y);
        set(gcf, 'Color', 'w');
        set(gca,'FontSize',axisfont);
        xlim([xmin xmax]*scale_x);
        ylim([ymin ymax]*scale_y);
        hold on;
        grid on;
        ylabel(lby,'FontSize',lbfont);
        xlabel(lbx,'FontSize',lbfont);
    end
end