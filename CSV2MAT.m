function [DATA] = CSV2MAT(n,ADDR,FILENAME)
    opts = delimitedTextImportOptions("NumVariables", n);
    
    % 指定范围和分隔符
    opts.DataLines = [1, Inf];
    opts.Delimiter = ",";
    
    x=char(zeros(1,2*n));
    j=1;
    % 指定列名称和类型
    for i=1:2*n
        if i<10
            x([j,j+1,j+2])=strcat(',x',int2str(i));
            j=j+3;
        else
            x([j,j+1,j+2,j+3])=strcat(',x',int2str(i));
            j=j+4;
        end
            
    end
    x=strsplit(x,',');
    for i=2:2*n+1
        opts.VariableNames(i-1)= x(i);
        opts.VariableTypes(i-1) =cellstr("double");
    end
    
    % 指定文件级属性
    opts.ExtraColumnsRule = "ignore";
    opts.EmptyLineRule = "read";
    DATA=readtable([ADDR,FILENAME],opts);
    % 导入数据
    DATA = table2array(readtable([ADDR,FILENAME], opts));
    DATA = DATA(2:size(DATA,1),:);
    clear opts
end