%% Made by Yiyang Liu
% 07/May/2022
%
% save the CSVMAT and plotgraph files, add them in the path
%
% For Plotting Cadence figure in MATLAB
% Please first save the cadence plot in .csv format, change the variables,
%   then use the function plotgraph to plot
%
% Feature:
%   adjustable linewidth
%   adjustable font size
%   fast plot
%   flexible number of signals
%
% Required Package: export_fig
%% 
clear all;
clc;
close all;

%% example
set(0,'DefaultLineLineWidth',2); %set linewidth
signum = 2; %number of signals
FOLDERNAME = '/home/s1906039/Desktop/'; %change foldername
FILENAME = 'delayvswn1'; %change filename
Xname='w2'; %name of the x axis
Xunit='m'; %unit of the x axis
Yname='Delay'; %name of the y axis
Yunit='s'; %unit of the y axis
axisfont=16; %axisfont
lbfont=18; %labelfont
fig=plotgraph(signum,FOLDERNAME,FILENAME,Xname,Xunit,Yname,Yunit,axisfont,lbfont); %get the handle of the figure
legend('\DeltaT_{rising}','\DeltaT_{falling}','Location','east','FontSize',16); %set the legend
export_fig('filename',[FOLDERNAME,'png/',FILENAME]); %export figure
