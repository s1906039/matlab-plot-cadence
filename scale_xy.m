function [scale_x,scale_unitx] = scale_xy(num)
    if num<1e2 && num>=1e-1
        scale_x=1;
        scale_unitx='';
    elseif num<1e-1 && num>=1e-3
        scale_x=1e3;
        scale_unitx='m';
    elseif num<1e-3 && num>=1e-6
        scale_x=1e6;
        scale_unitx='\mu';
    elseif num<1e-6 && num>=1e-9
        scale_x=1e9;
        scale_unitx='n';
    elseif num<1e-9 && num>=1e-12
        scale_x=1e12;
        scale_unitx='p';
    elseif num<1e3 && num>=1e2
        scale_x=1e-3;
        scale_unitx='k';
    elseif num<1e6 && num>=1e3
        scale_x=1e-6;
        scale_unitx='M';
    elseif num<1e9 && num>=1e6
        scale_x=1e-9;
        scale_unitx='G';
    end
end

